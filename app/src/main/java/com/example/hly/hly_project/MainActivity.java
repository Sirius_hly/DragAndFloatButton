package com.example.hly.hly_project;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private int screenWidth;
    private int screenHeight;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.floatbutton);
        DisplayMetrics dm = getResources().getDisplayMetrics();
        screenWidth = dm.widthPixels;
        screenHeight = dm.heightPixels ;
       FloatingActionButton floatingActionButton= (FloatingActionButton)findViewById(R.id.floatBtn);
       floatingActionButton.setOnTouchListener(movingEventListener);
    }
    private View.OnTouchListener movingEventListener = new View.OnTouchListener() {
        int lastX, lastY;

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    lastX = (int) event.getRawX();
                    lastY = (int) event.getRawY();
                    break;
                case MotionEvent.ACTION_MOVE:
                    int dx = (int) event.getRawX() - lastX;
                    int dy = (int) event.getRawY() - lastY;
                    int left = v.getLeft() + dx;
                    int top = v.getTop() + dy;
                    int right = v.getRight() + dx;
                    int bottom = v.getBottom() + dy;
                    if (left < 0) {
                        left = 0;
                        right = left + v.getWidth();
                    }
                    if (right > screenWidth) {
                        right = screenWidth;
                        left = right - v.getWidth();
                    }
                    if (top < 0) {
                        top = 0;
                        bottom = top + v.getHeight();
                    }
                    if (bottom > screenHeight) {
                        bottom = screenHeight;
                        top = bottom - v.getHeight();
                    }
                    v.layout(left, top, right, bottom);
                    lastX = (int) event.getRawX();
                    lastY = (int) event.getRawY();

                    RelativeLayout.LayoutParams lpFeedback=new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
                    lpFeedback.leftMargin=v.getLeft();
                    lpFeedback.topMargin=v.getTop();
                    lpFeedback.setMargins(v.getLeft(),v.getTop(),0,0);
                    v.setLayoutParams(lpFeedback);
                    break;
                case MotionEvent.ACTION_UP:
                    int a = Math.abs((int)event.getRawX()-lastX);
                    int b =Math.abs((int)event.getRawY()-lastY);
                    if(a==0&&b==0) {

                        Toast.makeText(getApplicationContext(), "拖动按钮，无视父件", Toast.LENGTH_SHORT).show();
                    }
				/*if(v.getLeft()<200||v.getRight()<200||v.getTop()<200||v.getBottom()<200){
				}*/
                    break;
            }
            return true;
        }
    };
}
